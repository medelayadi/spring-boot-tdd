package org.mel.tdd.tdddemo.controller;

import org.mel.tdd.tdddemo.dto.ProductDto;
import org.mel.tdd.tdddemo.exception.ProductNotFoundException;
import org.mel.tdd.tdddemo.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/products")
public class ProductController {
    @Autowired
    IProductService productService;

    @GetMapping("/{id}")
    public ResponseEntity<ProductDto> getProductById(
        @PathVariable final Long id
    ) {
        return ResponseEntity.ok(productService.getProduct(id));
    }
    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void ProductNotFoundHandler(ProductNotFoundException e) {

    }
}
