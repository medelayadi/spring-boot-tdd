package org.mel.tdd.tdddemo.service;

import org.mel.tdd.tdddemo.dto.ProductDto;

public interface IProductService {
    ProductDto getProduct(long anyLong);
}
