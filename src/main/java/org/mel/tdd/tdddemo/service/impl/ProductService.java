package org.mel.tdd.tdddemo.service.impl;

import org.mel.tdd.tdddemo.dto.ProductDto;
import org.mel.tdd.tdddemo.exception.ProductNotFoundException;
import org.mel.tdd.tdddemo.model.Product;
import org.mel.tdd.tdddemo.repository.IProductRepository;
import org.mel.tdd.tdddemo.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class ProductService implements IProductService {
    @Autowired
    private IProductRepository productRepository;

    @Override
    public ProductDto getProduct(long productId) {
        final Product byId = productRepository.findById(productId);
        if (Objects.isNull(byId)) {
            throw new ProductNotFoundException();
        }
        return ProductDto.fromProduct(byId);
    }
}
