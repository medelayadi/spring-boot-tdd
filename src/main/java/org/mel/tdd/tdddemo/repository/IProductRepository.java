package org.mel.tdd.tdddemo.repository;

import org.mel.tdd.tdddemo.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Mohamed EL AYADI on 01-Feb-19
 */
public interface IProductRepository extends JpaRepository<Product, Long> {
    Product findById(long anyLong);
}
