package org.mel.tdd.tdddemo.dto;

import lombok.*;
import org.mel.tdd.tdddemo.model.Product;

import java.util.Objects;

@Data
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto {
    private Long id;
    private String name;
    private CategoryDto category;

    public static ProductDto fromProduct(final Product product) {
        if (Objects.isNull(product)) {
            return null;
        }
        return ProductDto.builder()
            .id(product.getId())
            .name(product.getName())
            .category(CategoryDto.fromCategory(product.getCategory()))
            .build();
    }
}
