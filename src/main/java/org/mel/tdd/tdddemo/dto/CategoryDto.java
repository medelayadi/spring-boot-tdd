package org.mel.tdd.tdddemo.dto;

import lombok.*;
import org.mel.tdd.tdddemo.model.Category;

import java.util.Objects;

@Data
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class CategoryDto {
    private Long id;
    private String name;

    public static CategoryDto fromCategory(Category category) {
        if (Objects.isNull(category)) {
            return null;
        }
        return CategoryDto.builder()
            .id(category.getId())
            .name(category.getName())
            .build();
    }
}
