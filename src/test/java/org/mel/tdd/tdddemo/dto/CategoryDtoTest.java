package org.mel.tdd.tdddemo.dto;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mel.tdd.tdddemo.model.Category;

import static org.junit.Assert.*;

/**
 * Created by Mohamed EL AYADI on 01-Feb-19
 */
public class CategoryDtoTest {

    @Test
    public void fromCategory_shouldReturnNull() {
        Assertions.assertThat(CategoryDto.fromCategory(null)).isNull();
    }
    @Test
    public void fromCategory_shouldReturnNestedObject() {
        final Category category = new Category(1L, "category1");
        Assertions.assertThat(CategoryDto.fromCategory(category))
            .isEqualToComparingFieldByField(new CategoryDto().builder()
                .id(category.getId())
                .name(category.getName())
                .build()
            );
    }
}
