package org.mel.tdd.tdddemo.dto;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.mel.tdd.tdddemo.model.Category;
import org.mel.tdd.tdddemo.model.Product;

import static org.junit.Assert.*;

/**
 * Created by Mohamed EL AYADI on 01-Feb-19
 */
public class ProductDtoTest {

    @Test
    public void fromProduct_shouldReturnNull() {
        Assertions.assertThat(ProductDto.fromProduct(null)).isNull();
    }
    @Test
    public void fromProduct_shouldReturnExactObject() {
        final Product product = new Product(1L, "Toto", new Category(2L, "categ"));
        Assertions.assertThat(ProductDto.fromProduct(product)).isEqualToComparingFieldByField(
            new ProductDto(1l, "Toto", new CategoryDto(2L, "categ"))
        );
    }
}
