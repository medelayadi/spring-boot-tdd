package org.mel.tdd.tdddemo.repository;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mel.tdd.tdddemo.model.Category;
import org.mel.tdd.tdddemo.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class IProductRepositoryTest {
    @Autowired
    private IProductRepository productRepository;

    @Test
    public void findById_shouldReturnNestedProductOrNull() {
        //given
        productRepository.save(new Product(1L, "toto", new Category(1L, "categ")));
        //when
        final Product product = productRepository.findById(1L);
        //then
        Assertions.assertThat(product.getId()).isEqualTo(1L);
        Assertions.assertThat(product.getName()).isEqualTo("toto");
        Assertions.assertThat(product.getCategory().getId()).isEqualTo(1L);
        Assertions.assertThat(product.getCategory().getName()).isEqualTo("categ");
        Assertions.assertThat(productRepository.findById(2L)).isNull();
    }
}
