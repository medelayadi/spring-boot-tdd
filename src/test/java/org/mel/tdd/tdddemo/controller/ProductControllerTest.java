package org.mel.tdd.tdddemo.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mel.tdd.tdddemo.dto.CategoryDto;
import org.mel.tdd.tdddemo.dto.ProductDto;
import org.mel.tdd.tdddemo.exception.ProductNotFoundException;
import org.mel.tdd.tdddemo.service.IProductService;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private IProductService productService;

    @Test
    public void getProduct_shouldReturnProductDetails() throws Exception {
        //given
        BDDMockito.given(productService.getProduct(BDDMockito.anyLong()))
            .willReturn(new ProductDto(1L, "product1", new CategoryDto(1L, "category1")));
        //then
        mockMvc.perform(MockMvcRequestBuilders.get("/products/1"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("id").value(1L))
            .andExpect(jsonPath("name").value("product1"))
            .andExpect(jsonPath("category.id").value(1L))
            .andExpect(jsonPath("category.name").value("category1"));
    }
    @Test
    public void getProduct_shouldThrowNotFound() throws Exception {
        //given
        BDDMockito.given(productService.getProduct(BDDMockito.anyLong()))
            .willThrow(new ProductNotFoundException());

        //then
        mockMvc.perform(MockMvcRequestBuilders.get("/products/1"))
            .andExpect(status().isNotFound());
    }

}
