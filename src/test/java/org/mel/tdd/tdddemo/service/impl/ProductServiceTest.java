package org.mel.tdd.tdddemo.service.impl;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mel.tdd.tdddemo.dto.ProductDto;
import org.mel.tdd.tdddemo.exception.ProductNotFoundException;
import org.mel.tdd.tdddemo.model.Category;
import org.mel.tdd.tdddemo.model.Product;
import org.mel.tdd.tdddemo.repository.IProductRepository;
import org.mel.tdd.tdddemo.service.IProductService;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {
    @Mock
    private IProductRepository productRepository;
    @InjectMocks
    private ProductService productService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getProduct_shouldReturnProduct() {
        //given
        given(productRepository.findById(anyLong()))
            .willReturn(new Product(1L, "toto", new Category(1L, "categ")));
        //when
        final ProductDto productDto = productService.getProduct(1L);
        //then
        Assertions.assertThat(productDto.getId()).isEqualTo(1L);
        Assertions.assertThat(productDto.getName()).isEqualTo("toto");
        Assertions.assertThat(productDto.getCategory().getId()).isEqualTo(1L);
        Assertions.assertThat(productDto.getCategory().getName()).isEqualTo("categ");
    }

    @Test(expected = ProductNotFoundException.class)
    public void getProduct_shouldThrowProductNotFound() {
        //given
        given(productRepository.findById(anyLong()))
            .willReturn(null);
        //when
        final ProductDto productDto = productService.getProduct(1L);
    }
}
